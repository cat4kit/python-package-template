# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

#  test the created .gitlab-ci.yml file for different setups.
#
# note that all of these tests require the following environment variables:
#
# CI_API_V4_URL: The url for the gitlab API (is set automatically in Gitlab CI)
# GITLAB_API_TOKEN: A token with api scope to access the gitlab API and lint the files
#
# If any of these variables are not set, all the tests here are skipped

setup() {
    load 'test_helper/common-setup'
    _common_setup


    if [ ! "$CI_API_V4_URL" ]; then
      skip "Skipping due to missing gitlab API url"
    fi

    if [ ! "$GITLAB_API_TOKEN" ]; then
        skip "Skipping due to missing gitlab API token"
    fi

}

@test "test linting the gitlab ci (no deploy)" {
    create_template '{deploy_package_in_ci: "no", deploy_pages_in_ci: "no"}'
    lint_gitlab_ci
}

@test "test linting the gitlab ci (package deploy)" {
    create_template '{deploy_package_in_ci: "yes", deploy_pages_in_ci: "no"}'
    lint_gitlab_ci
}

@test "test linting the gitlab ci (pages deploy)" {
    create_template '{deploy_package_in_ci: "no", deploy_pages_in_ci: "yes"}'
    lint_gitlab_ci
}

@test "test linting the gitlab ci (default)" {
    create_template
    lint_gitlab_ci
}
