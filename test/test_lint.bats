# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "test linting the code" {
    create_template
    setup_venv
    make -C ${PROJECT_FOLDER} lint
}
