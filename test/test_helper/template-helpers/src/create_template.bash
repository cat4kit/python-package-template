# create_template
# ===============
#
# Summary: Create the template with a certain configuration
#
# Usage: create_template <config>

# Options:
#   <config>    The JSON-encoded configuration that you want to test
#
# IO:
#   STDERR - the failed expression, on failure
# Globals:
#   none
# Returns:
#   0 - if the template has been created successfully
#   1 - otherwise
#
#   ```bash
#   @test 'create()' {
#     create_template '{"some_param": "test"}'
#   }
#   ```
#

# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

create_template() {
  echo "default_context: $1" > "$BATS_TEST_TMPDIR/config.yaml"
  cookiecutter --no-input --config-file "$BATS_TEST_TMPDIR/config.yaml" . --output-dir $BATS_TEST_TMPDIR
  git -C ${PROJECT_FOLDER} commit --no-gpg-sign -m "Initial commit"
}
