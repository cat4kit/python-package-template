# lint_gitlab_ci
# ==============
#
# Summary: Lint the created gitlab_ci file against the GitHub API
#
# Usage: lint_gitlab_ci
#
# IO:
#   STDERR - the failed expression, on failure
# Globals:
#   CI_API_V4_URL
#   GITLAB_API_TOKEN
# Returns:
#   0 - if the .gitlab-ci.yml file is valid
#   1 - otherwise
#
#   ```bash
#   @test 'venv()' {
#     create_template
#     lint_gitlab_ci
#   }
#   ```
#

# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

lint_gitlab_ci() {
    # validate the created gitlab-ci file via the gitlab API,
    # see https://docs.gitlab.com/ee/api/lint.html
    run bats_pipe jq --null-input --arg yaml "$(<${PROJECT_FOLDER}/.gitlab-ci.yml)" '.content=$yaml' \
        \| curl "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/ci/lint" \
        --header 'Content-Type: application/json' \
        --header "Authorization: Bearer $GITLAB_API_TOKEN" \
        --data @- \
        \| jq --raw-output

    assert_output --partial '"valid": true'
}
