# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

source "$(dirname "${BASH_SOURCE[0]}")/src/create_template.bash"
source "$(dirname "${BASH_SOURCE[0]}")/src/setup_venv.bash"
source "$(dirname "${BASH_SOURCE[0]}")/src/lint_gitlab_ci.bash"
