# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
from pathlib import Path

from sphinx.ext import apidoc

sys.path.insert(0, os.path.abspath(".."))

if not os.path.exists("_static"):
    os.makedirs("_static")

# isort: off

import {{ cookiecutter.package_folder }} # noqa: E402

# isort: on


def generate_apidoc(app):
    appdir = Path(app.__file__).parent
    apidoc.main(
        ["-fMEeTo", str(api), str(appdir), str(appdir / "migrations" / "*")]
    )


api = Path("api")

if not api.exists():
    generate_apidoc({{ cookiecutter.package_folder }})

# -- Project information -----------------------------------------------------

project = "{{ cookiecutter.project_slug }}"
copyright = "{{ cookiecutter.copyright_year }} {{ cookiecutter.copyright_holder }}"
author = "{{ cookiecutter.project_authors }}"


linkcheck_ignore = [
    # we do not check link of the {{ cookiecutter.project_slug }} as the
    # badges might not yet work everywhere. Once {{ cookiecutter.project_slug }}
    # is settled, the following link should be removed
    r'https://.*{{ cookiecutter.project_slug }}'
]


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "hereon_nc_sphinxext",
    "autodoc_traits",
    "sphinx_copybutton",
    "sphinx.ext.autodoc",
    "sphinxext.opengraph",
    "sphinxext.rediraffe",
    "sphinx.ext.intersphinx",
    "sphinx_design",
    "sphinx.ext.napoleon",
    "sphinx.ext.todo",
    "sphinx.ext.autosectionlabel",
    "sphinx.ext.autosummary",
    "sphinx.ext.doctest",
    "sphinx.ext.extlinks",
    "sphinx.ext.coverage",
    "sphinx.ext.viewcode",
    "sphinxcontrib.jquery",
    {% if cookiecutter.use_jupyter_for_documentation == "yes" %}
    "nbsphinx",
    {%- endif %}
    {% if cookiecutter.use_markdown_for_documentation == "yes" %}"myst_parser"{%- endif %}
]

{% if cookiecutter.use_markdown_for_documentation == "yes" %}
myst_enable_extensions = ["fieldlist"]

source_suffix = {
    ".rst": "restructuredtext",
    ".md": "markdown",
}
{% endif %}
{% if cookiecutter.use_jupyter_for_documentation == "yes" %}
nbsphinx_execute = "never"
{% endif %}
# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]


autodoc_default_options = {
    "show_inheritance": True,
    "members": True,
    "autosummary": True,
}


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
{% if cookiecutter.select_sphinx_theme == "rtd" %}
html_theme = "sphinx_rtd_theme"

html_theme_options = {
    "collapse_navigation": False,
    "includehidden": False,
}
{% endif %}

{% if cookiecutter.select_sphinx_theme == "pydata" %}
html_theme = "pydata_sphinx_theme"
html_theme_options = {
    # "logo": {
    #     "image_light": "_static/tds2staclight.png",
    #     "image_dark": "https://codebase.helmholtz.cloud/cat4kit/ds2stac/tds2stac/-/raw/main/docs/_static/tds2stacdarks.svg",  # noqa
    # },
    # "use_edit_page_button": True,
    # "footer_start": ["footer"],
    # "footer_end": [
    #     "sphinx-version",
    #     "theme-version",
    #     "last-updated",
    # ],
    "gitlab_url": "{{ cookiecutter.git_remote_protocoll }}://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}",
    # "external_links": [
    #     {"name": "TDS2STAC", "url": "https://tds2stac.readthedocs.io/"},
    #     {"name": "INTAKE2STAC", "url": "https://intake2stac.readthedocs.io/"}, # noqa
    #     {"name": "STA2STAC", "url": "https://sta2stac.readthedocs.io/"},
    #     {
    #         "name": "INSUPDEL4STAC",
    #         "url": "https://insupdel4stac.readthedocs.io/",
    #     },
    # ],
    # "icon_links": [
    #     {
    #         "name": "DS2STAC",
    #         "url": "https://ds2stac.readthedocs.io",
    #         "icon": "_static/ds2stac-logo-light.png",
    #         "type": "local",
    #         # Add additional attributes to the href link.
    #         "attributes": {
    #             "target": "_blank",
    #             "rel": "noopener me",
    #             "class": "nav-link custom-fancy-css",
    #         },
    #     },
    #     {
    #         "name": "Cat4KIT",
    #         "url": "https://cat4kit.readthedocs.io",
    #         "icon": "_static/cat4kitlogo.png",
    #         "type": "local",
    #         # Add additional attributes to the href link.
    #         "attributes": {
    #             "target": "_blank",
    #             "rel": "noopener me",
    #             "class": "nav-link custom-fancy-css",
    #         },
    #     },
    # ],
}

html_context = {
    "edit_page_url_template": {% raw %}"https://{{ codebase_url }}/\
{{ codebase_user }}/{{ codebase_repo }}/blob/\
{{ codebase_version }}/{{ doc_path }}{{ file_name }}"{% endraw %},
    "codebase_url": "{{ cookiecutter.gitlab_host }}",
    "codebase_user": "{{ cookiecutter.gitlab_username }}",
    "codebase_repo": "{{ cookiecutter.project_slug }}",
    "codebase_version": "main",
    "doc_path": "docs",
}
{% endif %}
# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]


intersphinx_mapping = {
    "python": ("https://docs.python.org/3/", None),
    "django": ("https://django.readthedocs.io/en/stable/", None),
}
