"""{{ cookiecutter.project_title }}

{{ cookiecutter.project_short_description }}
"""

from __future__ import annotations

from . import _version

__version__ = _version.get_versions()["version"]

__author__ = "{{ cookiecutter.project_authors }}"
__copyright__ = "{{ cookiecutter.copyright_year }} {{ cookiecutter.copyright_holder }}"
__credits__ = [{% for author in cookiecutter.project_authors.split(',') %}
    "{{ author.strip()}}",
{% endfor %}]
__license__ = "EUPL-1.2"

__maintainer__ = "{{ cookiecutter.project_maintainers }}"
__email__ = "{{ cookiecutter.project_author_emails }}"

__status__ = "Pre-Alpha"
