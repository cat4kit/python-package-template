# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

"""Jinja2 extensions."""
from typing import List, Dict

from itertools import zip_longest, count

from jinja2.ext import Extension


class UnderlinedExtension(Extension):
    """Jinja2 extension to create a random string."""

    def __init__(self, environment):
        """Jinja2 Extension Constructor."""
        super().__init__(environment)

        def underlined(title, *args, symbol="-", above=False):
            title = title.format(*args)
            bar = symbol * len(title)
            if above:
                return f"{bar}\n{title}\n{bar}"
            else:
                return f"{title}\n{bar}"

        def get_author_infos(
            authors: List[str], emails: List[str]
        ) -> List[Dict[str, str]]:
            """Get author information from the authors and emails."""
            return [
                {
                    "full_name": full_name.strip(),
                    "first_name": full_name.rsplit(" ", 1)[0].strip(),
                    "last_name": full_name.rsplit(" ", 1)[0].strip(),
                    "email": email.strip(),
                }
                for full_name, email in zip_longest(authors, emails)
            ]

        environment.globals.update(
            get_author_infos=get_author_infos,
            underlined=underlined,
            count=count,
        )

        environment.filters["next"] = next
